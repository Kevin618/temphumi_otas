#include "esp_common.h"
#include "queue.h"
#include "freertos/FreeRTOS.h"




typedef struct
{
    int8_t H_Value;
    int8_t L_Value;
} HumiTemp;

enum sensor_type
{
  SENSOR_DHT11,
  SENSOR_DHT22
};
bool readDHT(enum sensor_type st,uint8 pin,int16_t *humidity, int16_t *temperature);
