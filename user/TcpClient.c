#include "esp_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "espressif/espconn.h"

#include "driver/uart.h"
#include "lwip/sys.h"
#include "TcpClient.h"
#include "freertos/queue.h"
#include "Led.h"

uint8 upgradeFlag = 0;
char *upgradeBuf = "UpgradeFirmware:http://www.sdhqxny.com/web/FirmWare/";


//const uint8 tcp_server_ip[4] = { 58, 58, 135, 78};
const uint8 tcp_server_ip[4] = { 192, 168, 1, 19};
#define TCP_SERVER_REMOTE_PORT (7007)
#define TCP_CLIENT_GREETING "Hello!This is a tcp client test\n"

#define TCP_CLIENT_KEEP_ALIVE_ENABLE 1

#define TCP_CLIENT_KEEP_ALIVE_IDLE_S (10)
#define TCP_CLIENT_RETRY_INTVL_S (5)
#define TCP_CLIENT_RETRY_CNT     (3)


#define SERVER_IP       "58.58.135.78"
#define SERVER_PORT  50000



void TcpClientConnect(void *arg)
{
  struct espconn* tcp_server_local = arg;
#if TCP_CLIENT_KEEP_ALIVE_ENABLE
  espconn_set_opt(tcp_server_local,BIT(3));//enable keep alive this api must call in connect callback
  uint32 keep_alive = 0;
  keep_alive = TCP_CLIENT_KEEP_ALIVE_IDLE_S;
  espconn_set_keepalive(tcp_server_local,ESPCONN_KEEPIDLE,&keep_alive);
  keep_alive = TCP_CLIENT_RETRY_INTVL_S;
  espconn_set_keepalive(tcp_server_local,ESPCONN_KEEPINTVL,&keep_alive);
  keep_alive = TCP_CLIENT_RETRY_CNT;
  espconn_set_keepalive(tcp_server_local,ESPCONN_KEEPCNT,&keep_alive);
  DBG_PRINT("keep alive enable\n");
  #endif
  DBG_LINES("TCP CLIENT CONNECT");
  DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
    espconn_send(tcp_server_local, TCP_CLIENT_GREETING, strlen(TCP_CLIENT_GREETING));
}
void TcpClientDisConnect(void *arg)
{
struct espconn* tcp_server_local = arg;
DBG_LINES("TCP CLIENT DISCONNECT");
DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
}
void TcpClienSendCb(void* arg)
{
    struct espconn* tcp_server_local = arg;
    DBG_LINES("TCP CLIENT SendCb");
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
}
void TcpRecvCb(void *arg, char *pdata, unsigned short len)
{
    struct espconn* tcp_server_local = arg;
   char *buf = (char *)zalloc(64);

    DBG_PRINT("Recv tcp client ip:%d.%d.%d.%d port:%d len:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port, len);

   	DBG_PRINT("tcp recv msg:%s!\r\n" , pdata );
	if( strlen( pdata ) > 0 )
	{
	   printf("tcp recv msg:%s!\r\n" , pdata );

	  if( strcmp( pdata , "turn on led") == 0 )
	  {
		led_on( );
	  }
	 else if( strcmp( pdata , "turn off led") == 0 )
	{
	   led_off( );
	}
	 else if( strcmp( pdata , "Version") == 0 )
	 {
	       sprintf(buf,"compile time:%s %s,SDK version:%s,ESP8266 chip ID:0x%x\r\n", __DATE__, __TIME__,system_get_sdk_version( ),system_get_chip_id());
	 	espconn_send(tcp_server_local, buf, strlen(buf));
	 }
	else if( strncmp( pdata , "UpgradeFirmware:" , 16 ) == 0 ) 
	{
	   upgradeFlag = 1;
	   strcpy(upgradeBuf, pdata);
	   DBG_PRINT("upgradeBuf recv msg:%s!\r\n" , upgradeBuf );
	   DBG_PRINT("upgradeFlag recv msg:%d!\r\n" , upgradeFlag );

	}
     }
  
	free(buf);
    espconn_send(tcp_server_local, pdata, len);
	free( pdata );
       
	//vTaskDelete( NULL );
}
void TcpReconnectCb(void *arg, sint8 err)
{
     struct espconn* tcp_server_local = arg;
    DBG_LINES("TCP CLIENT RECONNECT");
    DBG_PRINT("status:%d\n", err);
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
}
void TCPLocalClient(void *arg)
{
   uint8_t con_status = wifi_station_get_connect_status();
   while(con_status != STATION_GOT_IP){
       con_status = wifi_station_get_connect_status();
	//DBG_PRINT("TCP CLIENT RECONNECT");
	DBG_PRINT("Connect ap %s\n",
                con_status==STATION_IDLE?"IDLE":con_status==STATION_CONNECTING? "Connecting...":con_status==STATION_WRONG_PASSWORD? "Password":con_status==STATION_NO_AP_FOUND? "ap_not_find":con_status==STATION_CONNECT_FAIL?"Connect fail":"Get ip");
	vTaskDelay(100);
   }
   //static struct espconn tcp_client;
   static esp_tcp tcp;
   tcp_client.type = ESPCONN_TCP;
   tcp_client.proto.tcp = &tcp;
   tcp.remote_port = TCP_SERVER_REMOTE_PORT;
   memcpy(tcp.remote_ip,tcp_server_ip,sizeof(tcp_server_ip));
   espconn_regist_connectcb(&tcp_client,TcpClientConnect);
   espconn_regist_recvcb(&tcp_client,TcpRecvCb);
   espconn_regist_reconcb(&tcp_client,TcpReconnectCb);
   espconn_regist_disconcb(&tcp_client,TcpClientDisConnect);
   espconn_regist_sentcb(&tcp_client,TcpClienSendCb);
   os_printf("tcp client connect server,server ip:%d.%d.%d.%d port:%d\n", tcp_client.proto.tcp->remote_ip[0],
            tcp_client.proto.tcp->remote_ip[1], tcp_client.proto.tcp->remote_ip[2], tcp_client.proto.tcp->remote_ip[3],
            tcp_client.proto.tcp->remote_port);
   espconn_connect(&tcp_client);
   vTaskDelete(NULL);
}
void TcpServerClientConnect(void*arg)
{
    struct espconn* tcp_server_local = arg;
#if TCP_SERVER_KEEP_ALIVE_ENABLE
    espconn_set_opt(tcp_server_local, BIT(3)); //enable keep alive ,this api must call in connect callback

    uint32 keep_alvie = 0;
    keep_alvie = TCP_SERVER_KEEP_ALIVE_IDLE_S;
    espconn_set_keepalive(tcp_server_local, ESPCONN_KEEPIDLE, &keep_alvie);
    keep_alvie = TCP_SERVER_RETRY_INTVL_S;
    espconn_set_keepalive(tcp_server_local, ESPCONN_KEEPINTVL, &keep_alvie);
    keep_alvie = keep_alvie = TCP_SERVER_RETRY_INTVL_S;
    espconn_set_keepalive(tcp_server_local, ESPCONN_KEEPCNT, &keep_alvie);
    DBG_PRINT("keep alive enable\n");
#endif
    DBG_LINES("TCP server CONNECT");
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
    espconn_send(tcp_server_local, TCP_SERVER_GREETING, strlen(TCP_SERVER_GREETING));
}
void TcpServerClientDisConnect(void* arg)
{
    struct espconn* tcp_server_local = arg;
    DBG_LINES("TCP server DISCONNECT");
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
}

void TcpServerClienSendCb(void* arg)
{
    struct espconn* tcp_server_local = arg;
    DBG_LINES("TCP server SendCb");
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port);
}

void TcpServerRecvCb(void *arg, char *pdata, unsigned short len)
{
    struct espconn* tcp_server_local = arg;
    DBG_PRINT("Recv tcp client ip:%d.%d.%d.%d port:%d len:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port, len);
    espconn_send(tcp_server_local, pdata, len);
}
void TcpServerReconnectCb(void *arg, sint8 err)
{
    struct espconn* tcp_server_local = arg;
    DBG_LINES("TCP server RECONNECT");
    DBG_PRINT("status:%d\n", err);
    DBG_PRINT("tcp client ip:%d.%d.%d.%d port:%d\n", tcp_server_local->proto.tcp->remote_ip[0],
            tcp_server_local->proto.tcp->remote_ip[1], tcp_server_local->proto.tcp->remote_ip[2],
            tcp_server_local->proto.tcp->remote_ip[3], tcp_server_local->proto.tcp->remote_port\
);
}

void TcpLocalServer(void* arg)
{

    static struct espconn tcp_server_local;
    static esp_tcp tcp;
    tcp_server_local.type = ESPCONN_TCP;
    tcp_server_local.proto.tcp = &tcp;
    tcp.local_port = TCP_SERVER_LOCAL_PORT;

    espconn_regist_connectcb(&tcp_server_local, TcpServerClientConnect);
    espconn_regist_recvcb(&tcp_server_local, TcpServerRecvCb);
    espconn_regist_reconcb(&tcp_server_local, TcpServerReconnectCb);
    espconn_regist_disconcb(&tcp_server_local, TcpServerClientDisConnect);
    espconn_regist_sentcb(&tcp_server_local, TcpServerClienSendCb);

    espconn_accept(&tcp_server_local);
    vTaskDelete(NULL);
}
/*
void UdpRecvCb(void *arg, char *pdata, unsigned short len)
{
    struct espconn* udp_server_local = arg;
    DBG_LINES("UDP_RECV_CB");
    DBG_PRINT("UDP_RECV_CB len:%d ip:%d.%d.%d.%d port:%d\n", len, udp_server_local->proto.tcp->remote_ip[0],
            udp_server_local->proto.tcp->remote_ip[1], udp_server_local->proto.tcp->remote_ip[2],
            udp_server_local->proto.tcp->remote_ip[3], udp_server_local->proto.tcp->remote_port\
);
    espconn_send(udp_server_local, pdata, len);
}
void UdpSendCb(void* arg)
{
    struct espconn* udp_server_local = arg;
    DBG_LINES("UDP_SEND_CB");
    DBG_PRINT("UDP_SEND_CB ip:%d.%d.%d.%d port:%d\n", udp_server_local->proto.tcp->remote_ip[0],
            udp_server_local->proto.tcp->remote_ip[1], udp_server_local->proto.tcp->remote_ip[2],
            udp_server_local->proto.tcp->remote_ip[3], udp_server_local->proto.tcp->remote_port\
);
}

void udpServer(void*arg)
{
    static struct espconn udp_client;
    static esp_udp udp;
    udp_client.type = ESPCONN_UDP;
    udp_client.proto.udp = &udp;
    udp.local_port = UDP_SERVER_LOCAL_PORT;

    espconn_regist_recvcb(&udp_client, UdpRecvCb);
    espconn_regist_sentcb(&udp_client, UdpSendCb);
    int8 res = 0;
    res = espconn_create(&udp_client);
    if (res != 0) {
        DBG_PRINT("UDP SERVER CREAT ERR ret:%d\n", res);
    }
    vTaskDelete(NULL);
}

os_timer_t time1;
static struct espconn udp_client;

void t1Callback(void* arg)
{
    os_printf("t1 callback\n");
    espconn_send(&udp_client, TCP_CLIENT_GREETING, strlen(TCP_CLIENT_GREETING));

}
void udpClient(void*arg)
{

    static esp_udp udp;
    udp_client.type = ESPCONN_UDP;
    udp_client.proto.udp = &udp;
    udp.remote_port = UDP_SERVER_LOCAL_PORT;
    memcpy(udp.remote_ip, udp_server_ip, sizeof(udp_server_ip));

    espconn_regist_recvcb(&udp_client, UdpRecvCb);
    espconn_regist_sentcb(&udp_client, UdpSendCb);
    int8 res = 0;
    res = espconn_create(&udp_client);

    if (res != 0) {
        DBG_PRINT("UDP SERVER CREAT ERR ret:%d\n", res);
    }

    os_timer_disarm(&time1);
    os_timer_setfn(&time1, t1Callback, NULL);
    os_timer_arm(&time1, 1000, 1);

    vTaskDelete(NULL);

}*/

