#include "DHT11_RTOS.h"
#include "driver/gpio.h"

#define DHT_TIMER_INTERVAL 2
#define DHT_DATA_BITS        40

//#define DEBUG_DHT
#ifdef DEBUG_DHT
#define debug(fmt,...)printf("%s"fmt"\n","dht:",##__VA_ARGS__);
#else
#define debug(fmt,...)/**do nothing*/
#endif
     //pin timeout pin_state   get tiem of the pin hingh or low
//xQueueHandle publish_queue;



static bool dht_await_pin_state(uint8_t pin,uint32_t timeout,
                                            bool expected_pin_state,uint32_t *duration)
{
uint32_t i ;
   for (i = 0; i < timeout; i += DHT_TIMER_INTERVAL)
   {
   	//need to wait at leadt a single intervel to prevent reading a jitter
   	os_delay_us(DHT_TIMER_INTERVAL);
	if(GPIO_INPUT_GET(pin) == expected_pin_state)
	{
	   if(duration)
	   {
	      *duration = i;
	   }
	   return true;
	}
   }
   return false;
}
static inline bool pollDHTCb(int pin,bool bits[DHT_DATA_BITS])
{
  uint32_t low_duration;
  uint32_t high_duration;

  //set the output to hight to hance the high-low change for wakeup
  //not need really,there is pullup resistor
  GPIO_OUTPUT_SET(pin,1);
  os_delay_us(20*1000);

  //DHT wakeup signal hold low for 20ms(datasheet ata least 18ms)
  GPIO_OUTPUT_SET(pin,0);
  os_delay_us(20*1000);

  //disable output function to receive low response from dht
  GPIO_DIS_OUTPUT(pin);

  if(!dht_await_pin_state(pin,40,false,NULL))
  {
     debug("DHT not response with low after wakeup.\n");
	 return false;
  }
  //Step through phase c 88us (datasheet 80us)
  if(!dht_await_pin_state(pin,88,true,NULL))
  	{
  	debug("DHT not response with high during intialiazation.\n");
	return false;
  	}
   //Step through phase c 88us (datasheet 80us)
  if(!dht_await_pin_state(pin,88,false,NULL))
  	{
  	debug("DHT not response with low during intialiazation.\n");
	return false;
  	}
  //read in each of the 40 bits oa data...
  int i;
  for(i = 0;i<DHT_DATA_BITS;i++)
  {
  	if(!dht_await_pin_state(pin,65,true,&low_duration))
  	{
  	   debug("DHT not response hight to transimit 1bit \n");
	   return false;
  	}
	if(!dht_await_pin_state(pin,75,false,&high_duration))
	{
	  debug("FHT not response low to finish transmit 1bit and start next cycle\n");
	  return false;
	}
	//to start transmit 1bit dht response low for 50us
	//bit 0 is identified by high for 26-28us
	//bit 1 is identified by high for 70us
	bits[i]= high_duration > low_duration;
  }
  return true;
}
static inline int16_t dht_convert_data(enum sensor_type st,uint8_t msb,uint8_t lsb)
{
  int16_t data;

  if(st == SENSOR_DHT22)
  {
      data = msb&0x7f;
      data<<=8;
      data |= lsb;
	if(msb&BIT(7))
	{
	data = 0-data; //conver it to nagative
	}
  }
  else
  	{
  	data = msb*10+lsb;
  	}
  return data;
}
bool readDHT(enum sensor_type st,uint8 pin,int16_t *humidity, int16_t *temperature)
{
bool bits[DHT_DATA_BITS];
uint8_t data[DHT_DATA_BITS/8]={0};
bool result;

taskENTER_CRITICAL();
result = pollDHTCb(pin,bits);
taskEXIT_CRITICAL();

if(!result)
 {
  return false;
 }
uint8_t i;
for (i =0;i<DHT_DATA_BITS;i++)
{
  //read each bit into result byte array...
  data[i/8]<<=1;
  data[i/8] |= bits[i];
}
if(data[4]!=((data[0]+data[1]+data[2]+data[3])&0xff))
{
 debug("Checksum failed invalid data received from sensor\n");
 return false;
}
*humidity = dht_convert_data(st,data[0],data[1]);
*temperature = dht_convert_data(st,data[2],data[3]);

debug("sensor data: humidty=%d,temp=%d\n",*humidity,*temperature);
return true;
}

