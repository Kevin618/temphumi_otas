/*
 * ESPRSSIF MIT License
 *
 * Copyright (c) 2015 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on ESPRESSIF SYSTEMS ESP8266 only, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "esp_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "espressif/espconn.h"
#include "espressif/airkiss.h"
#include "driver/uart.h"
#include "driver/gpio.h"

#include "TcpClient.h"

#include "Led.h"
#include "UdpServer.h"
#include "upgrade.h"
#include "DHT11_RTOS.h"

//#define server_ip "192.168.101.142"
//#define server_port 9669



#ifndef ADD2STR
#define ADD2STR(a)  (a)[2], (a)[3],(a)[4], (a)[5]
#define ADDSTR "%02d:%02d:%02d:%02d"
#endif


uint8 macuid[6];

char ToServerBuf[] = {};
uint8 LinkOverFlag = 0;
void led_toggle_task( void *pvParameters );

#define DEVICE_TYPE 		"gh_9e2cff3dfa51" //wechat public number
#define DEVICE_ID 			"122475" //model ID

#define DEFAULT_LAN_PORT 	12476

LOCAL esp_udp ssdp_udp;
LOCAL struct espconn pssdpudpconn;
LOCAL os_timer_t ssdp_time_serv;

uint8  lan_buf[200];
uint16 lan_buf_len;
uint8  udp_sent_cnt = 0;

uint8 LinkCount = 0;

char UserBinStr[10];
xTaskHandle key_handler_task_handle;
xTaskHandle led_toggle_task_handle;

typedef struct
{
    uint8 type;
    int16_t value;
} measurement;



const airkiss_config_t akconf =
{
	(airkiss_memset_fn)&memset,
	(airkiss_memcpy_fn)&memcpy,
	(airkiss_memcmp_fn)&memcmp,
	0,
};

LOCAL void ICACHE_FLASH_ATTR
airkiss_wifilan_time_callback(void)
{
	uint16 i;
	airkiss_lan_ret_t ret;
	
	if ((udp_sent_cnt++) >30) {
		udp_sent_cnt = 0;
		os_timer_disarm(&ssdp_time_serv);//s
		//return;
	}

	ssdp_udp.remote_port = DEFAULT_LAN_PORT;
	ssdp_udp.remote_ip[0] = 255;
	ssdp_udp.remote_ip[1] = 255;
	ssdp_udp.remote_ip[2] = 255;
	ssdp_udp.remote_ip[3] = 255;
	lan_buf_len = sizeof(lan_buf);
	ret = airkiss_lan_pack(AIRKISS_LAN_SSDP_NOTIFY_CMD,
		DEVICE_TYPE, DEVICE_ID, 0, 0, lan_buf, &lan_buf_len, &akconf);
	if (ret != AIRKISS_LAN_PAKE_READY) {
		os_printf("Pack lan packet error!");
		return;
	}
	
	ret = espconn_sendto(&pssdpudpconn, lan_buf, lan_buf_len);
	if (ret != 0) {
		os_printf("UDP send error!");
	}
	os_printf("Finish send notify!\n");
}

LOCAL void ICACHE_FLASH_ATTR
airkiss_wifilan_recv_callbk(void *arg, char *pdata, unsigned short len)
{
	uint16 i;
	remot_info* pcon_info = NULL;
		
	airkiss_lan_ret_t ret = airkiss_lan_recv(pdata, len, &akconf);
	airkiss_lan_ret_t packret;
	
	switch (ret){
	case AIRKISS_LAN_SSDP_REQ:
		espconn_get_connection_info(&pssdpudpconn, &pcon_info, 0);
		os_printf("remote ip: %d.%d.%d.%d \r\n",pcon_info->remote_ip[0],pcon_info->remote_ip[1],
			                                    pcon_info->remote_ip[2],pcon_info->remote_ip[3]);
		os_printf("remote port: %d \r\n",pcon_info->remote_port);
      
        pssdpudpconn.proto.udp->remote_port = pcon_info->remote_port;
		memcpy(pssdpudpconn.proto.udp->remote_ip,pcon_info->remote_ip,4);
		ssdp_udp.remote_port = DEFAULT_LAN_PORT;
		
		lan_buf_len = sizeof(lan_buf);
		packret = airkiss_lan_pack(AIRKISS_LAN_SSDP_RESP_CMD,
			DEVICE_TYPE, DEVICE_ID, 0, 0, lan_buf, &lan_buf_len, &akconf);
		
		if (packret != AIRKISS_LAN_PAKE_READY) {
			os_printf("Pack lan packet error!");
			return;
		}

		os_printf("\r\n\r\n");
		for (i=0; i<lan_buf_len; i++)
			os_printf("%c",lan_buf[i]);
		os_printf("\r\n\r\n");
		
		packret = espconn_sendto(&pssdpudpconn, lan_buf, lan_buf_len);
		if (packret != 0) {
			os_printf("LAN UDP Send err!");
		}
		
		break;
	default:
		os_printf("Pack is not ssdq req!%d\r\n",ret);
		break;
	}
}

void ICACHE_FLASH_ATTR
airkiss_start_discover(void)
{
	ssdp_udp.local_port = DEFAULT_LAN_PORT;
	pssdpudpconn.type = ESPCONN_UDP;
	pssdpudpconn.proto.udp = &(ssdp_udp);
	espconn_regist_recvcb(&pssdpudpconn, airkiss_wifilan_recv_callbk);
	espconn_create(&pssdpudpconn);

	os_timer_disarm(&ssdp_time_serv);
	os_timer_setfn(&ssdp_time_serv, (os_timer_func_t *)airkiss_wifilan_time_callback, NULL);
	os_timer_arm(&ssdp_time_serv, 1000, 1);//1s
}


void ICACHE_FLASH_ATTR
smartconfig_done(sc_status status, void *pdata)
{
    switch(status) {
        case SC_STATUS_WAIT:
            printf("SC_STATUS_WAIT\n");
            break;
        case SC_STATUS_FIND_CHANNEL:
            printf("SC_STATUS_FIND_CHANNEL\n");
	     LinkOverFlag = 1;
            break;
        case SC_STATUS_GETTING_SSID_PSWD:
            printf("SC_STATUS_GETTING_SSID_PSWD\n");
            sc_type *type = pdata;
            if (*type == SC_TYPE_ESPTOUCH) {
                printf("SC_TYPE:SC_TYPE_ESPTOUCH\n");
            } else {
                printf("SC_TYPE:SC_TYPE_AIRKISS\n");
            }
            break;
        case SC_STATUS_LINK:
            printf("SC_STATUS_LINK\n");
            struct station_config *sta_conf = pdata;
	
	        wifi_station_set_config(sta_conf);
	        wifi_station_disconnect();
	        wifi_station_connect();
            break;
        case SC_STATUS_LINK_OVER:
            printf("SC_STATUS_LINK_OVER\n");
            if (pdata != NULL) {
				//SC_TYPE_ESPTOUCH
                uint8 phone_ip[4] = {0};
           
                memcpy(phone_ip, (uint8*)pdata, 4);
                printf("Phone ip: %d.%d.%d.%d\n",phone_ip[0],phone_ip[1],phone_ip[2],phone_ip[3]);
            } else {
            	//SC_TYPE_AIRKISS - support airkiss v2.0
				airkiss_start_discover();
			}
			LinkOverFlag = 0;
			
            smartconfig_stop();
            break;
    }
	
}

void ICACHE_FLASH_ATTR
smartconfig_task(void *pvParameters)
{
    smartconfig_start(smartconfig_done);
    
    vTaskDelete(NULL);
}
/**
    * @brief  no .    
    * @note   no.
    * @param  no.
    * @retval no.
    */
void key_int_handler( void )
{
	uint32 gpio_status;

	gpio_status = GPIO_REG_READ( GPIO_STATUS_ADDRESS );

	GPIO_REG_WRITE( GPIO_STATUS_W1TC_ADDRESS , gpio_status );

	if( gpio_status & (BIT(0)) )
	{
		xTaskResumeFromISR( key_handler_task_handle );
	}
}
/**
    * @brief  no .    
    * @note   no.
    * @param  no.
    * @retval no.
    */
void key_init( void )
{
	GPIO_ConfigTypeDef	GPIOConfig;
	
	GPIOConfig.GPIO_Pin = GPIO_Pin_0;
	GPIOConfig.GPIO_Mode = GPIO_Mode_Input;
	GPIOConfig.GPIO_Pullup = GPIO_PullUp_DIS;
	GPIOConfig.GPIO_IntrType = GPIO_PIN_INTR_NEGEDGE;
	gpio_config( &GPIOConfig );
	
	gpio_intr_handler_register( key_int_handler , NULL );

	_xt_isr_unmask(1 << 4);
}
/**
    * @brief  no .    
    * @note   no.
    * @param  no.
    * @retval no.
    */
void key_handler_task( void *pvParameters )
{
    uint32_t TickCountPre = 0 , TickCountCur = 0;

	key_init();
	
	for( ;; )
	{
		vTaskSuspend( NULL );

		TickCountPre = TickCountCur;
			
		TickCountCur = xTaskGetTickCount( );
		if( TickCountCur - TickCountPre > 7 )
		{
			uint8_t i;

			for( i = 0; i < 10 ; i ++ )
			{
				vTaskDelay( 500 / portTICK_RATE_MS );

				uint32_t gpio_value;

				gpio_value = gpio_input_get( );
				if( ( gpio_value & BIT(0) ) == BIT(0) )
				{
					break;
				}
			}

			if( i == 10 )
			{
				smartconfig_start( smartconfig_done );
			}
		}
			
	}
}

/**
    * @brief  no .    
    * @note   no.
    * @param  no.
    * @retval no.
    */
void led_toggle_task( void *pvParameters )
{
  static uint8 delaycount = 0;
	for( ;; )
	{
	      if(LinkOverFlag == 0)
		  GPIO_OUTPUT( GPIO_Pin_4 ,0x00000001 );	
		else if(LinkOverFlag == 1)	
		{
		   led_toggle( );
		   printf("%s is running!\r\nled toggle!\r\n" , UserBinStr );
		}
		  
		  vTaskDelay( 500 / portTICK_RATE_MS );	
		delaycount++;
	   if(delaycount>=8) ////4S�������һ��
	   {
	   	if(upgradeFlag == 1&&upgradeBuf>0)
              {
                 if( strncmp( upgradeBuf , "UpgradeFirmware:" , 16 ) == 0 ) 
				{
					if( ExcuteUpgrade( (char *)&upgradeBuf[16] ) == true )
					{
						//send( client_sock , "Upgrade is Excuted!\r\n" , strlen( "Upgrade is Excuted!\r\n" ) , 0 );
						upgradeFlag = 0;
						printf("Upgrade is Excuted!\r\n");
					}
					else
					{
					printf("Upgrade fail!\r\n");
						//send( client_sock , "Upgrade fail!\r\n" , strlen( "Upgrade fail!\r\n" ) , 0 );
					}
                 }
             }
	   }
	}
	
	vTaskDelete(NULL);
}

void dht_task(void *pvParameters)
{
  //set GPIO 14 to output mode for dht11
  PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U,FUNC_GPIO14);

  int16_t humi;
  int16_t temp;

  measurement hmsg;
  measurement tmsg;
  bool readvalue;

  hmsg.type = 1;
  tmsg.type = 2;
  while(1)
  {
  readvalue=readDHT(SENSOR_DHT11,14,&humi,&temp);
  if(true == readvalue)
  {
    os_printf("Humidity : %d Temperature: %d \n",humi,temp);
    hmsg.value = humi;
    tmsg.value = temp;

   sprintf(ToServerBuf, "hqTempHumi,hquid:%02%02x%02x%02x,TempH:%02f,humi:%02f,hqTempHumiEnd", ADD2STR((char *)macuid), temp, humi);
    //espconn_send(tcp_server_local_A, ToServerBuf, strlen(ToServerBuf));
  }
  else
  	{
  	os_printf("error reading dht11 value...\n");
  	}
  sint8 espconn_accept_state =  espconn_connect(&tcp_client);
  if(espconn_accept_state != ESPCONN_ISCONN)
  	{
  	LinkCount++;
	if(LinkCount>10)
	 {
	 os_printf("TCP connnect fail...\n");
	 vTaskDelay(1000);
	TCPLocalClient();
	 }
  	}
  //three seconds delay...
  vTaskDelay(5000/portTICK_RATE_MS);
  }
}





void TcpLocalClient_Test()
{
     printf("TCP .......................................................................................................\r\n"  );
	vTaskDelay(1000);
	TCPLocalClient();
}
/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_init(void)
{
  espconn_init();
	user_uart_init( );
	 printf("ESP8266 chip ID:0x%x\n",system_get_chip_id());
       printf("SDK version:%s\n", system_get_sdk_version( ) );
	wifi_get_macaddr(0x01, macuid);
	if ( system_upgrade_userbin_check( ) == UPGRADE_FW_BIN1 ) 
	{
		strncpy(UserBinStr , "user1.bin" , 9 );
	}
	else
	{
		strncpy(UserBinStr , "user2.bin" , 9 );
	}
	UserBinStr[9] = '\0';
	printf( "%s is running!\r\n" , UserBinStr );
	led_init( );
	
    /* need to set opmode before you set config */
    wifi_set_opmode(STATION_MODE);

//    xTaskCreate(smartconfig_task, "smartconfig_task", 256, NULL, 2, NULL);
	xTaskCreate(led_toggle_task, "led_toggle_task", 256, NULL, 1, NULL);
	xTaskCreate(key_handler_task, "key_handler_task", 256, NULL, 3, &key_handler_task_handle );
	//printf("ESP8266 chip ID.............................................................................................................................................:0x%x\n",system_get_chip_id());
       xTaskCreate(dht_task, "dht11", 256, NULL, 2, NULL);
	//    printf("ESP8266 chip ID.............................................................................................................................................:0x%x\n",system_get_chip_id());
	 xTaskCreate(TcpLocalClient_Test,"TcpLocalClient_Test",500,NULL,7,NULL);
	//UdpServerInit();
	tcp_server_thread_init( );

}

